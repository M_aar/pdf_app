# =============================================================================
# Testing PDF files
#
#
# =============================================================================
import numpy as np
import PyPDF2 as pdf
import IO
#from tabula import read_pdf
#import camelot


num_pages =  [9]
content = ''
pdf_file = open("D:/Python/pdf_app/data/BMW_X6_PriceList.pdf", "rb")

read_pdf = pdf.PdfFileReader(pdf_file)


for i in range(0, 13):

    if not i in num_pages: continue
    content = read_pdf.getPage(i).extractText().splitlines()
    print(content)


    #content = ' ' .join(content.replace(u"\xa0", "").strip().split())
#print(content)

pdf_file.close()

## Using the tabula
#test = read_pdf("D:/Python/pdf_app/data/BMW_X6_PriceList.pdf")
#print(test)
#
## Using the camelot:
#tables = camelot.read_pdf("D:/Python/pdf_app/data/BMW_X6_PriceList.pdf")
#print(tables)

# Let us use pycpdf
#import pycpdf